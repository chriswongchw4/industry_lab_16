package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */

    Course _adaptee;
    DistributionPanel distributionPanel;

    public DistributionPanelAdapter(DistributionPanel distributionPanel) {
        this.distributionPanel = distributionPanel;
    }

    @Override
    public void courseHasChanged(Course course) {
        this._adaptee = course;
        distributionPanel.repaint();
    }
}
