package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
    Course _adaptee;

    public CourseAdapter(Course courseModel) {
        this._adaptee = courseModel;
        courseModel.addCourseListener(this);
    }

    /**********************************************************************
     * YOUR CODE HERE
     */


    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }


    @Override
    public int getRowCount() {

        return _adaptee.size();
    }

    @Override
    public int getColumnCount() {

        return 7;
    }

    @Override
    public String getColumnName(int column) {


        switch (column){
            case 0:
                return "StudentID";
            case 1:
                return "Surname";
            case 2:
                return "Forename";
            case 3:
                return "Exam";
            case 4:
                return "Test";
            case 5:
                return "Assignment";
            case 6:
                return "Overall";

            default:
                return super.getColumnName(column);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        StudentResult r = _adaptee.getResultAt(rowIndex);


        if (r != null) {
            switch (columnIndex) {
                case 0:
                    return r._studentID;
                case 1:
                    return r._studentSurname;
                case 2:
                    return r._studentForename;
                case 3:
                    return r.getAssessmentElement(StudentResult.AssessmentElement.Exam);
                case 4:
                    return r.getAssessmentElement(StudentResult.AssessmentElement.Test);
                case 5:
                    return r.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
                case 6:
                    return r.getAssessmentElement(StudentResult.AssessmentElement.Overall);
                default:
                    return null;
            }
        }

        return null;
    }
}